# ideas

A place to discuss ideas about the tnt-phoenix project.

Un luogo dove discutere delle idee riguardo al progetto tnt-phoenix.

It's possible to discuss ideas about the tnt-phoenix project also using the
Etherpad document available [HERE](https://pad.riseup.net)
(append `/p/icHc2BiU7oVQ` to the URL to get the real document).

È possibile discutere delle idee riguardo al progetto tnt-phoenix anche
utilizzando il documento Etherpad disponibile [QUI](https://pad.riseup.net)
(aggiungere `/p/icHc2BiU7oVQ` in coda all'URL per ottenere il vero documento).
